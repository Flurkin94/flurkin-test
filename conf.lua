-- chikun :: 2015
-- The configuration file for our tutorial


--[[
    The function which runs at the start of the game, and
    allows us to modify the game window
  ]]
function love.conf(game)

    game.window.width   = 1920
    game.window.height  = 1080
    game.window.title   = "For All Lost, Love Outside Uniformed Tears"
    game.window.resizable = true

    --[[
        On Windows, will attach a debug console to the game.
        Set to false before distribution.
      ]]
    game.console = true

end
