-- chikun :: 2014
-- Template state


-- Temporary state, removed at end of script
local playState = { }


-- On state create
function playState:create()

    view = {
        x = 0,
        y = 0,
        w = 854,
        h = 480
    }

    map.current = maps['test']

    -- load up the oobjects on map
    for key,layer in ipairs(map.current.layers) do

        -- load up all collidable objects
        if layer.name == 'collisions' then
            collisions = layer.objects

        -- load up all important objects
        elseif layer.name == 'important' then

            for key,object in ipairs(layer.objects) do

                -- create the player
                if object.name == 'spawn' then
                    player.set(object.x, object.y, object.w, object.h)
                end

            end

        elseif layer.name == 'enemies' then

            for key,object in ipairs(layer.objects) do

                -- create the enemies
                enemy.set(object.x, object.y, object.w, object.h)
            end

        elseif layer.name == 'platform' then

            for key, object in ipairs(layer.objects) do

                platform = layer.objects

            end

        end

        layer.objects = { }

    end

    -- sets background color to black
    g.setBackgroundColor(0,0,0)

end


-- On state update
function playState:update(dt)

    -- calls function update for player
    player.update(dt)

    enemy.update(dt)

    -- Moves camera
    view.x = math.clamp(0, player.x + player.w / 2 - view.w / 2,
        map.current.w * map.current.tileW - view.w)
    view.y = math.clamp(0, player.y + player.h / 2 - view.h / 2,
        map.current.h * map.current.tileH - view.h)

    sfx.level0:play()

end


-- On state draw
function playState:draw()

    g.origin()

    g.scale(g.getWidth() / 854, g.getHeight() / 480)

    g.translate(-view.x, -view.y)

    -- draws map
    map.draw()

    -- draws enemies
    enemy.draw()

    -- draws player
    player.draw()

    g.origin()

end


-- On state kill
function playState:kill()

end


-- Transfer data to state loading script
return playState
