-- chikun :: 2014
-- Template state


-- Temporary state, removed at end of script
local graphicsState = { }


-- On state create
function graphicsState:create()

    -- Set background colour to black
    g.setBackgroundColor(0,0,0)

    -- Menu selection
    menuSelection = 1

    -- Fullscreen
    fullscreenKey = 1
    fullscreen = {
        "No",
        "Yes"
    }

    -- Options
    menuOptions = {
        "Fullscreen",
        "Back"
    }

end


-- On state update
function graphicsState:update(dt)

end


-- On key press
function graphicsState:onKey(key)

    -- if Space is pushed
    if key == " " then

        -- checks what is selected then changes
        if menuSelection == 1 then

            fullscreenKey = fullscreenKey + 1

            if fullscreenKey > 2 then
                fullscreenKey = 1
            end

            if fullscreenKey == 1 then
                w.setFullscreen(false)
            end

            if fullscreenKey == 2 then
                w.setFullscreen(true, "desktop")
            end

        elseif menuSelection == 2 then

            state.change(states.options)

        end

    -- moves down on menuSelection
    elseif key == "down" then

        menuSelection = menuSelection + 1

        if menuSelection == 3 then

            menuSelection = 1

        end

    -- moves up on menuSelection
    elseif key == "up" then

        menuSelection = menuSelection - 1

        if menuSelection == 0 then

            menuSelection = 2

        end

    end

end


-- On state draw
function graphicsState:draw()

    -- for loop to draw the menuOptions
    for key, value in ipairs(menuOptions) do

        -- used to change text variables
        local text = menuOptions[key]

        if key == 1 then
            text = text .. ": " .. fullscreen[fullscreenKey]
        end

        g.setColor(100,100,100)

        if menuSelection == key then

            g.setColor(255, 255, 255)

        end

        g.printf(text, 0, 100 * key, 854, 'center')

    end

end


-- On state kill
function graphicsState:kill()

end


-- Transfer data to state loading script
return graphicsState
