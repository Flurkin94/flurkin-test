-- chikun :: 2014
-- Template state


-- Temporary state, removed at end of script
local graphicsState = { }


-- On state create
function graphicsState:create()

    -- Set background colour to black
    g.setBackgroundColor(0,0,0)

    -- Menu selection
    menuSelection = 1

    -- Master Volume
    master = 10

    -- Music Volume
    music = 10

    -- SFX
    sefx = 10

    -- Options
    menuOptions = {
        "Master",
        "BGM",
        "SFX",
        "Back"
    }

end


-- On state update
function graphicsState:update(dt)

end


-- On key press
function graphicsState:onKey(key)

    -- if right is pushed
    if key == "right" then

        -- checks what is selected then increases it
        if menuSelection == 1 then

            master = master + 1

            if master > 10 then
                master = 0
            end

            a.setVolume((master / 10))

        elseif menuSelection == 2 then

            music = music + 1

            if music > 10 then
                music = 0
            end

            bgm:setVolume((music / 10))


        elseif menuSelection == 3 then

            sefx = sefx + 1

            if sefx > 10 then
                sefx = 0
            end

            sfx:setVolume((sefx / 10))

        end


    elseif key == "left" then

        -- checks what is selected then decreases it
        if menuSelection == 1 then

            master = master - 1

            if master < 0 then
                master = 10
            end

            a.setVolume((master / 10))

        elseif menuSelection == 2 then

            music = music - 1

            if music < 0 then
                music = 10
            end

            bgm:setVolume((music / 10))

        elseif menuSelection == 3 then

            sefx = sefx - 1

            if sefx < 0 then
                sefx = 10
            end

            sfx:setVolume((sefx / 10))

        end

    -- moves down on menuSelection
    elseif key == "down" then

        menuSelection = menuSelection + 1

        if menuSelection == 5 then

            menuSelection = 1

        end

    -- moves up on menuSelection
    elseif key == "up" then

        menuSelection = menuSelection - 1

        if menuSelection == 0 then

            menuSelection = 4

        end

    -- Used to go back to options
    elseif key == " " then

            if menuSelection == 4 then

                state.change(states.options)

            end

        end

end


-- On state draw
function graphicsState:draw()

    -- for loop to draw the menuOptions
    for key, value in ipairs(menuOptions) do

        -- used to change text variables
        local text = menuOptions[key]

        if key == 1 then
            text = text .. ": " .. master .. "0%"
        end

        if key == 2 then
            text = text .. ": " .. music .. "0%"
        end

        if key == 3 then
            text = text .. ": " .. sefx .. "0%"
        end

        g.setColor(100,100,100)

        if menuSelection == key then

            g.setColor(255, 255, 255)

        end

        g.printf(text, 0, 85 * key, 854, 'center')

    end

end


-- On state kill
function graphicsState:kill()

end


-- Transfer data to state loading script
return graphicsState
