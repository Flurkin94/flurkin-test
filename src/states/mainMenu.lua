-- chikun :: 2014
-- Template state


-- Temporary state, removed at end of script
local menuState = { }


-- On state create
function menuState:create()

    -- Set background colour to black
    g.setBackgroundColor(0,0,0)

    -- Menu selection
    menuSelection = 1
    menuOptions = {
        "Play",
        "Options",
        "Exit"
    }

end


-- On state update
function menuState:update(dt)

end


-- On key press
function menuState:onKey(key)

    -- if Space is pushed
    if key == " " then

        -- checks what is selected then changes state
        if menuSelection == 1 then

            state.change(states.play)

        elseif menuSelection == 2 then

            state.change(states.options)

        elseif menuSelection == 3 then

            e.quit()

        end

    -- moves down on menuSelection
    elseif key == "down" then

        menuSelection = menuSelection + 1

        if menuSelection == 4 then

            menuSelection = 1

        end

    -- moves up on menuSelection
    elseif key == "up" then

        menuSelection = menuSelection - 1

        if menuSelection == 0 then

            menuSelection = 3

        end

    end

end


-- On state draw
function menuState:draw()

    -- for loop to draw the menuOptions
    for key, value in ipairs(menuOptions) do

        g.setColor(100,100,100)
        g.printf(menuOptions[key], 427, 100 * key, 0, 'center')

        if menuSelection == key then

            g.setColor(255, 255, 255)
            g.printf(menuOptions[key], 427, 100 * key, 0, 'center')

        end

    end

end


-- On state kill
function menuState:kill()

end


-- Transfer data to state loading script
return menuState
