-- chikun :: 2015
-- Player script



player = {}
collisions = {}
enemies = {}



-- Called when map loads
player.set = function(x, y, w, h)


    -- Set player position
    player.x = x ; player.y = y
    player.w = w ; player.h = h

    -- Set player image
    player.stillImageTable =
    {
        gfx.playerStill,
        gfx.playerStill2,
        gfx.playerStill3,
        gfx.playerStill4,
        gfx.playerStill5,
        gfx.playerMove,
        gfx.playerMove2
    }

    player.animStep = 0
    player.currentAnim = player.stillImageTable

    -- For player movement
    player.gravity      = 2048
    player.xMod         = 0
    player.yMod         = 0
    player.ySpeed       = 0
    player.ySpeedMax    = 512
    player.xMove = 0
    player.knockBackTimer = 0
    player.invulnerable = 0
    player.rollTimer = 0
    player.rollWait = 0
    player.fallThrough = false

    -- Set player score
    player.score = 0

    -- Variables for player attacks
    player.attackReady = false
    player.attackAnimation = 0
    player.direction = 1
    --[[
    player.hurtBox = {
        x = (player.x + player.w),
        y = (player.y + player.h / 2 - 20),
        w = 20,
        h = 30
    }]]
    player.hp = 100

    player.weapon = {
        damage = 10,
        attackTimer = 0.5,
        hurtBox = {
            x = (player.x + player.w),
            y = player.y + player.h / 2 - 20,
            w = 20,
            h = 30
        }
    }

    player.weapons = {
        fists = {
            damage = 10,
            attackTimer = 0.5,
            hurtBox = {
                x = (player.x + player.w),
                y = player.y + player.h / 2 - 20,
                w = 20,
                h = 30
            }
        },

        daggers = {
            damage = 15,
            attackTimer = 0.35,
            hurtBox = {
                x = (player.x + player.w),
                y = player.y + player.h / 2 - 20,
                w = 40,
                h = 15
            }
        },

        sword = {
            damage = 25,
            attackTimer = 0.75,
            hurtBox = {
                x = (player.x + player.w),
                y = player.y + player.h / 2 - 20,
                w = 50,
                h = 50
            }
        },

        greatSword = {
            damage = 50,
            attackTimer = 1.5,
            hurtBox = {
                x = (player.x + player.w),
                y = player.y + player.h / 2 - 20,
                w = 70,
                h = 70
            }
        },

        bow = {
            damage = 15,
            attackTimer = 0.5,
            hurtBox = {
                x = (player.x + player.w) + 600,
                y = player.y + player.h / 2 - 20,
                w = 20,
                h = 30
            }
        },

        axes = {
            damage = 20,
            attackTimer = 0.6,
            hurtBox = {
                x = (player.x + player.w),
                y = player.y + player.h / 2 - 20,
                w = 40,
                h = 50
            }
        }
    }


end





-- Called using update phase
player.update = function(dt)


    player.xMove = 0

    if player.direction == 1 then

        player.weapon.hurtBox.x = (player.x + player.w)

    end

    if player.direction == -1 then

        player.weapon.hurtBox.x = (player.x - player.weapon.hurtBox.w)

    end

    if player.hp == 0 then

    end
    -- Figure out which direction the player will move horizontally
    if input.check('left') and player.rollWait == 0 then
        player.xMove = -1
        player.direction = -1
        player.weapon.hurtBox.x = (player.x - player.weapon.hurtBox.w)
        player.animStep = (player.animStep + (dt * 8)) % #player.currentAnim
    end
    if input.check('right') and player.rollWait == 0 then
        player.xMove = player.xMove + 1
        player.direction = 1
        player.weapon.hurtBox.x = (player.x + player.w)
        player.animStep = (player.animStep + (dt * 8)) % #player.currentAnim
    end

    if player.xMove == 0 then
        if player.animStep > 1 then
            player.animStep = (player.animStep - (dt * 8)) % #player.currentAnim
        else player.animStep = 0
        end
    end


    -- Attack button calls attack function
    if input.check('action') and player.rollWait == 0 then
        player.attack()
    end

    if input.check('action1') and player.rollWait == 0 then
        player.roll()
    end
    if input.check('down') then
       player.weaponChange()
    end

    -- Actually move the player horizontal
    if player.rollTimer == 0 then
        player.x = player.x + 256 * dt * player.xMove +
        (player.xMod * dt * player.knockBackTimer)
    else
        player.x = player.x + 256 * dt * player.direction * 1.7
    end


    -- Check horizontal movement
    player.checkHorizontal(dt)


    -- Increase falling speed
    player.ySpeed = player.ySpeed + player.gravity * dt

    -- Restrict falling speed
    player.ySpeed = math.min(player.ySpeed, player.ySpeedMax)

    -- Actually make player fall
    player.y = player.y + player.ySpeed * dt +
        (player.yMod * dt * player.knockBackTimer)


    -- Check vertical movement
    player.checkVertical(dt)

    if player.invulnerable == 0 and player.rollTimer == 0 then

        for key, object in ipairs(enemies) do

            if math.overlap(enemies[key], player) then

                local dir = math.atan2((player.y + player.h / 2) - (object.y + object.h / 2), (player.x + player.w / 2) - (object.x + object.w / 2))

                player.xMod = math.cos(dir) * 600
                player.ySpeed= math.sin(dir) * 600

                player.invulnerable = 1

                player.knockBackTimer = 0.5

                player.hp = math.max(0, player.hp - object.damage)
            end
        end
    end

    -- Attack timers
    player.weapon.attackTimer      = math.max(0, player.weapon.attackTimer - (1 * dt))
    player.attackAnimation  = math.max(0, player.attackAnimation - (1 * dt))
    if player.attackAnimation == 0 then
        player.attackReady = false
    end

    player.weapon.hurtBox.y    = (player.y + player.h / 2 - 20)
    player.knockBackTimer   = math.max(0, player.knockBackTimer - (1 * dt))
    player.invulnerable     = math.max(0, player.invulnerable - (1 * dt))
    player.rollTimer        = math.max(0, player.rollTimer - (1 * dt))
    player.rollWait         = math.max(0, player.rollWait - (1 * dt))

end



-- Called using draw phase
player.draw = function()


    -- Draw player
    g.draw(player.currentAnim[math.floor(player.animStep) + 1], player.x + player.w / 2, player.y, 0, player.direction, 1, player.w / 2, 0)

    -- draws attack
    if player.attackReady then
        g.setColor(255,255,255)
        g.rectangle('fill', player.weapon.hurtBox.x, player.weapon.hurtBox.y, player.weapon.hurtBox.w, player.weapon.hurtBox.h)
    end


end



-- Check vertical movement
player.checkVertical = function(dt)


    -- Can the player jump?
    local canJump = false

    -- Check if top half of player colliding
    topHalf = {
        x = player.x,
        y = player.y,
        w = player.w,
        h = player.h / 2
    }

    -- While colliding...
    while (math.overlapTable(collisions, topHalf)) do

        -- Move box out of collision
        topHalf.y = math.floor(topHalf.y + 1)

        -- Set player's ySpeed to 0
        player.ySpeed = 0

    end


    -- Move player to determined y position
    player.y = topHalf.y


    -- Check if bottom half of player colliding
    bottomHalf = {
        x = player.x,
        y = player.y + player.h / 2,
        w = player.w,
        h = player.h / 2
    }

    -- While colliding...
    while (math.overlapTable(collisions, bottomHalf)) do

        -- Move box out of collision
        bottomHalf.y = math.ceil(bottomHalf.y - 1)

        -- Set player's ySpeed to 0
        player.ySpeed = 0

        -- Allow player to jump
        canJump = true

    end


    if not input.check('down') and not player.fallThrough and player.ySpeed > 0 then
        while (math.overlapTable(platform, bottomHalf)) do

            -- Move box out of collision
            bottomHalf.y = math.ceil(bottomHalf.y - 1)

            -- Set player's ySpeed to 0
            player.ySpeed = 0

            -- Allow player to jump
            canJump = true

        end

    elseif input.check('down') or player.fallThrough and player.rollWait == 0 then

        if math.overlapTable(platform, bottomHalf) then

            player.fallThrough = true

        else

            player.fallThrough = false

        end

    end

    -- Move player to determined y position
    player.y = bottomHalf.y - player.h / 2

    -- Make player jump if holding spacebar and can
    if (canJump and input.check("up") and player.rollWait == 0) then

        -- Up they go
        player.ySpeed = -768 * 1.2

    end


end



-- Check horizontal movement
player.checkHorizontal = function(dt)


    -- Check if left half of player colliding
    leftHalf = {
        x = player.x,
        y = player.y,
        w = player.w / 2,
        h = player.h
    }

    -- While colliding...
    while (math.overlapTable(collisions, leftHalf)) do

        -- Move box out of collision
        leftHalf.x = math.floor(leftHalf.x + 1)

    end


    -- Move player to determined x position
    player.x = leftHalf.x

    -- Check if right half of player colliding
    rightHalf = {
        x = player.x + player.w / 2,
        y = player.y,
        w = player.w / 2,
        h = player.h
    }

    -- While colliding...
    while (math.overlapTable(collisions, rightHalf)) do

        -- Move box out of collision
        rightHalf.x = math.floor(rightHalf.x - 1)

    end


    -- Move player to determined x position
    player.x = rightHalf.x - player.w / 2


end

-- Attack function
player.attack = function()

    -- if timer is at 0 then ready to attack
    if player.weapon.attackTimer == 0 then
        player.attackReady = true
        player.attackAnimation = 0.25
        player.weapon.attackTimer = 0.5

        -- checks collisions with enemies and takes hp off when hit.
        for key, object in ipairs(enemies) do

            if math.overlap(object, player.hurtBox) then
                object.hp = math.max(0, object.hp - player.damage)
            end

        end
    end

end

player.roll = function()

    player.rollTimer = 0.5
    player.rollWait = 1

end

player.weaponChange = function()
    player.weapon.damage = player.weapons.greatSword.damage
    player.weapon.attackTimer = player.weapons.greatSword.attackTimer
    player.weapon.hurtBox.x = player.weapons.greatSword.hurtBox.x
    player.weapon.hurtBox.y = player.weapons.greatSword.hurtBox.y
    player.weapon.hurtBox.w = player.weapons.greatSword.hurtBox.w
    player.weapon.hurtBox.h = player.weapons.greatSword.hurtBox.h

end
