-- chikun :: 2015
-- Enemy script


-- enemy tables
enemy = {}
enemies = {}

enemy.set = function(x,y,w,h)


    -- Set enemy position and details
    enemies[#enemies + 1] = {
        x = x,
        y = y,
        w = w,
        h = h,
        hp = 20,
        image = gfx.enemies,
        gravity = 2048,
        ySpeed = 0,
        ySpeedMax = 512,
        damage = 10
    }


end

-- updates enemies
enemy.update = function(dt)

    for key, object in ipairs(enemies) do

        local xMove = 0
        -- Actually move the player horizontal
        object.x = object.x + 256 * dt * xMove


        -- Check horizontal movement
        enemy.checkHorizontal(dt, object)


        -- Increase falling speed
        object.ySpeed = object.ySpeed + object.gravity * dt

        -- Restrict falling speed
        object.ySpeed = math.min(object.ySpeed, object.ySpeedMax)

        -- Actually make player fall
        object.y = object.y + object.ySpeed * dt


        -- Check vertical movement
        enemy.checkVertical(dt, object)

        if object.hp <= 0 then
            table.remove(enemies, key)
        end

    end



end

-- draws enemies
enemy.draw = function()

    for key, object in ipairs(enemies) do

        g.draw(object.image, object.x, object.y)
    end

end

enemy.checkVertical = function(dt, object)


    -- Can the player jump?
    local canJump = false

    -- Check if top half of player colliding
    topHalf = {
        x = object.x,
        y = object.y,
        w = object.w,
        h = object.h / 2
    }

    -- While colliding...
    while (math.overlapTable(collisions, topHalf)) do

        -- Move box out of collision
        topHalf.y = math.floor(topHalf.y + 1)

        -- Set player's ySpeed to 0
        object.ySpeed = 0

    end

    while (math.overlapTable(player, topHalf)) do

        -- Move box out of collision
        topHalf.y = math.floor(topHalf.y + 1)

        -- Set player's ySpeed to 0
        object.ySpeed = 0

    end

    -- Move player to determined y position
    object.y = topHalf.y


    -- Check if bottom half of player colliding
    bottomHalf = {
        x = object.x,
        y = object.y + object.h / 2,
        w = object.w,
        h = object.h / 2
    }

    -- While colliding...
    while (math.overlapTable(collisions, bottomHalf)) do

        -- Move box out of collision
        bottomHalf.y = math.ceil(bottomHalf.y - 1)

        -- Set player's ySpeed to 0
        object.ySpeed = 0

        -- Allow player to jump
        canJump = true

    end

    while (math.overlapTable(player, bottomHalf)) do

        -- Move box out of collision
        bottomHalf.y = math.ceil(bottomHalf.y - 1)

        -- Set player's ySpeed to 0
        object.ySpeed = 0

        -- Allow player to jump
        canJump = true

    end

    -- Move player to determined y position
    object.y = bottomHalf.y - object.h / 2

    -- Make player jump if holding spacebar and can
    if canJump then

        -- Up they go
        object.ySpeed = -768

    end


end



-- Check horizontal movement
enemy.checkHorizontal = function(dt, object)


    -- Check if left half of player colliding
    leftHalf = {
        x = player.x,
        y = player.y,
        w = player.w / 2,
        h = player.h
    }

    -- While colliding...
    while (math.overlapTable(collisions, leftHalf)) do

        -- Move box out of collision
        leftHalf.x = math.floor(leftHalf.x + 1)

    end

    while (math.overlapTable(enemies, leftHalf)) do

        -- Move box out of collision
        leftHalf.x = math.floor(leftHalf.x + 1)

    end

    -- Move player to determined x position
    player.x = leftHalf.x

    -- Check if right half of player colliding
    rightHalf = {
        x = player.x + player.w / 2,
        y = player.y,
        w = player.w / 2,
        h = player.h
    }

    -- While colliding...
    while (math.overlapTable(collisions, rightHalf)) do

        -- Move box out of collision
        rightHalf.x = math.floor(rightHalf.x - 1)

    end

    while (math.overlapTable(enemies, rightHalf)) do

        -- Move box out of collision
        rightHalf.x = math.floor(rightHalf.x - 1)

    end

    -- Move player to determined x position
    player.x = rightHalf.x - player.w / 2


end
