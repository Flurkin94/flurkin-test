-- chikun :: 2015
-- Weapon script


weapons =
{

    fists = {
        damage = 10,
        attackTimer = 0.5,
        hurtbox = {
        }
    }

    daggers = {
        damage = 15,
        attackTimer = 0.35,
        hurtbox = {
        }
    }

    sword = {
        damage = 25,
        attackTimer = 0.75,
        hurtbox = {
        }
    }

    greatSword = {
        damage = 50,
        attackTimer = 1.5,
        hurtbox = {
        }
    }

    bow = {
        damage = 15,
        attackTimer = 0.5,
        hurtbox = {
        }
    }

    axes = {
        damage = 20,
        attackTimer = 0.6,
        hurtbox = {
        }
    }

}

