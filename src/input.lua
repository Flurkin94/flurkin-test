-- Table of key tables
joy = {
    down =   { '2+' },
    left =   { '1-' },
    right =  { '1+' },
    up =     { '2-' },
    action = { 'b1', 'b11' },
    action1 = { 'b12' },
    menu =   { 'b10' },
    back =   { 'b2' },
}


-- Table of key tables
keys = {
    down =   { 's', 'down'  },
    left =   { 'a', 'left'  },
    right =  { 'd', 'right' },
    up =     { 'w', 'up'    },
    action = { ' ', 'return' },
    action1 = { 'z' },
    menu =   { 'escape' },
    back =   { 'lctrl', 'backspace' },
}
