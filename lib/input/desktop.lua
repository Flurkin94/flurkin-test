-- chikun :: 2014
-- Handles input from gamepad and keyboard


-- The table that houses input functions
input = { }


-- Load bindings
require "src/input"


-- Actually checks if a key is pressed
function input.check(inputType)

    -- Our return value
    local keyPressed = false

    -- Checks each key in a certain key table
    -- to see if it pressed
    for number, key in ipairs(keys[inputType]) do
        if k.isDown(key) then
            keyPressed = true
        end
    end

    -- Gather list of joysticks
    local list = j.getJoysticks()

    -- Check if joystick attached
    if #list > 0 then
        -- Assign first joystick to a variable
        local pad = list[1]

        -- Cycle through parts of joy table
        for key, joyCmd in ipairs(joy[inputType] or { }) do
            -- Recieve the command and the modifier
            local cmd, mod = joyCmd:sub(1, 1), joyCmd:sub(2)

            -- Work on the axes
            if cmd == "1" or cmd == "2" then
                -- By default, multiplier is 1...
                local multiplier = 1

                -- ...reverse if negative
                if mod == "-" then
                    multiplier = -1
                end

                -- Receive axis based on variables
                local axis = pad:getAxis(cmd) * multiplier

                -- THE CLAMPS
                axis = math.clamp(0, axis, 1)

                -- If pushed a proper amount...
                if axis > 0.4 then
                    -- ...report that the axis is pushed
                    keyPressed = true
                end
            -- Work on the buttons
            elseif cmd == "b" then
                -- Check dat button
                if pad:isDown(mod) then
                    keyPressed = true
                end
            end
        end
    end

    -- Return what we found out
    return keyPressed

end
